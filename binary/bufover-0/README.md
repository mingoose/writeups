# BufferOverflow #0

> 100 points
>
> The close cousin of a website for "Question marked as duplicate"
>
> Can you cause a segfault and get the flag?

A basic buffer overflow attack, send a long enough string and you'll get the
flag.
