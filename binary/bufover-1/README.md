# BufferOverflow #1

> 200 points
>
> The close cousin of a website for "Question marked as duplicate" - part 2!
>
> Can you redirect code execution and get the flag?

Now we need to control the return pointer - write the address of `win()` to
where the return address sits on the stack.
