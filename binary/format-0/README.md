# Format #0

> 200 points
>
> Someone didn't tell Chaddha not to give user input as the first argument to
> `printf()` - use it to leak the flag!

Since the first argument of `printf()` allows us to put format specifiers such
as `%s`, we can read arbitrary data off the stack. In this challenge, there's a
convenient pointer to the flag placed on the stack as the argument to `vuln()`,
so we just need enough `%s`'s until we get to it---or, if we calculate the
offset by examining the binary, we can figure out that we need the 24th (4 byte
wide) argument and use a single `%24$s`.
