# Format #1

> 250 points
>
> `printf` can do more than just read memory... can you change the variable?

As the description says, we need to chagne the value of `num`; luckily for us,
`printf` has `%n`, which writes the current number of bytes printed to a
pointer. `vuln()` hasn't changed since [the last
challenge](binary/format-0/README.md), so to perform the write we can use
`%24$n`. Now we just need to also have printf actually print 42 characters to
set `num` to 42, which we can do with `%42n`---print a number off the stack (we
don't care what), and pad it to be 42 characters wide.

