# Loopy #0

> 350 points
>
> This program is quite short, but has got `printf` and `gets` in it! This
> *shouldn't* be too hard, right?

We've got no `win()` this time around, we'll be needing a shell! As the title
hints towards, we need to loop back around into `vuln()` after we use the printf
vulnerability to leak a libc address. First, we craft a payload to dump an entry
of the GOT - I chose to use `printf`. We put the address of `printf@got` in the
beginning of our payload, then use `%4$s` to print out the GOT value as a string
(which we then unpack). Subtracting the address of `printf` in the provided libc
gives us the base address of libc, allowing us to calculate the address of any
function in libc. We also overflow the buffer and redirect execution back into
`vuln()`, giving us the chance to use the information we just leaked. With this,
we can execute a standard ret2libc attack, overflowing the buffer returning into
`system` and passing the address of `/bin/sh` within libc.

## Extra credit

The format string vulnerability isn't necessary to complete this challenge; we
can also return into `printf@plt` or `puts@plt` and pass a GOT entry as an
argument to leak a libc address, and have the `printf`/`puts` that we just
called return back into `vuln()`. The rest of the exploit remains the same.
