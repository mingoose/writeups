# Loopy #1

> 500 points
>
> Same program as Loopy #0, but someone's turned on the stack protector now!

With the stack canary enabled, we can't exploit any buffer overflows now, but we
still need to redirect code execution back into `vuln()`. However, when the
stack canary check fails, the program calls `__stack_chk_fail()`---a function in
libc with a GOT entry that we can overwrite on the first `printf()` call with
`vuln()`. pwntools has a utility to generate format string write payloads
automatically, but I've included a manual implementation here. Armed with a libc
base address, we just need to overwrite `printf()` with `system()` and send
`/bin/sh` on the next loop around to get a shell.

## Addendum - format string writes

We need to write some large values---`vuln()` is some 137 million bytes away
from `00000000`---so we need to get a little creative with our payload. Any
printf format specifier can take additional flags to specify the size of the
argument, so we can use `%hhn` to perform a one-byte write. Now, we can write
one byte at a time, in ascending order of value, and need much fewer characters
printed from printf. However, loading the 4 addresses in the string is already
16 bytes, so to write `0804`, the upper 2 bytes of `vuln()`, we'll need to
actually write `0x104` and `0x108`, since by the time we hit the `%n`'s we
already have the 16 bytes of addresses printed. Since `%hhn` prints bytes, it
chops off the high bits and just writes `0x04` and `0x08`.

If you don't want to bother with any of this, pwntools has got you covered too
:)
