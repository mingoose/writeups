import random
import socket
from pwn import *
# Attack from https://crypto.stackexchange.com/questions/62750/attack-of-the-middle-square-weyl-sequence-prng
# egcd function and modinv from https://en.wikibooks.org/wiki/Algorithm_Implementation/Mathematics/Extended_Euclidean_algorithm

def egcd(a, b):
    if a == 0:
        return (b, 0, 1)
    else:
        g, y, x = egcd(b % a, a)
        return (g, x - (b // a) * y, y)

def modinv(a, m):
    g, x, y = egcd(a, m)
    if g != 1:
        return -1
    else:
        return x % m

def mod64(num):
    return num % (1<<64)

def mod32(num):
    return num % (1<<32)

class weyl():
    def __init__(self, *args, **kwargs):
        if "x" in kwargs:
            self.x = kwargs["x"]
        else:
            self.x = 0x96354cfa25091e27
        if "w" in kwargs:
            self.w = kwargs["w"]
        else:
            self.w = 0xc839a0f4861bf843
        if "s" in kwargs:
            self.s = kwargs["s"]
        else:
            self.s = 0xb5ad4eceda1ce2a9

    def nextRand(self):
        self.w = mod64(self.w + self.s)
        self.x = mod64(self.x**2 + self.w)
        self.x = mod64((self.x << 32) + (self.x >> 32))
        return mod32(self.x)

def solve_congruence(coefficient, product, modulus = 1<<32):
    g = egcd(egcd(coefficient, product)[0], product)[0]
    inv = modinv(coefficient//g, modulus//g)
    if inv == -1:
        return []
    else:
        return [mod32(inv*product//g + k*modulus//g) for k in range(g)]

def solve(b, s):
    s_low = mod32(s)
    s_hi = s >> 32
    for e_0 in range(3):
        for e_1 in range(3):
            for e_2 in range(3):
                e = [e_0, e_1, e_2]
                product_guess = mod32((b[3] - b[2]) - (s_hi + e[2] + (b[2]**2 >> 32) - (b[1]**2 >> 32) + 2*b[2]*(b[1]**2+2*s_low) - 2*b[1]*(b[0]**2 + s_low)))
                coefficient = mod32((2*b[2] - 2*b[1]))
                for d_guess in solve_congruence(coefficient, product_guess):
                    c_guess = mod32(b[2] - ((b[1]**2 >>32) + 2*b[1]*(b[0]**2 + d_guess + s_low) + 2*s_hi + e[0] + e[1]))
                    product_guess = mod32(b[1] - (c_guess + (b[0]**2 >> 32) + s_hi + e[0]))
                    coefficient = mod32(2*b[0])
                    for a_guess in solve_congruence(coefficient, product_guess):
                        gen_predict = weyl(x = (a_guess << 32) + b[0], w = (c_guess << 32)+d_guess, s = s)
                        correct = True
                        b_guess = [b[0]] + [gen_predict.nextRand() for i in range(9)]
                        for i in range(10):
                            if b[i] != b_guess[i]:
                                correct = False
                                break
                        if not correct:
                            continue
                        return gen_predict

def send_solve(host, port, s):

    connection = remote(host, port)
    connection.send("")
    print (connection.recvuntil("> ").decode('utf-8'))
    b = []
    for i in range(10):
        connection.send("r\n")
        b.append(int(connection.recvuntil("> ")[:-3].decode('utf-8')))

    connection.send("g\n")
    gen_predict = solve(b, s)
    if gen_predict == None:
        print ("Unable to solve equations... run solve.py again!")
        return

    print (connection.recvuntil("> ").decode('utf-8'))
    for i in range(10):
        guess = str(gen_predict.nextRand())
        print (guess)
        connection.send(guess + "\n")
        print (connection.recvuntil("> ").decode('utf-8'))
    data = connection.recvuntil("}").decode('utf-8')
    print (data)

send_solve("shell.2019.nactf.com", 31382, 0xb5ad4eceda1ce2a9)
