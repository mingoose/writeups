# Dr. J's Group Test Randomizer: Board Problem #0

> Dr. J created a fast pseudorandom number generator (prng) to randomly assign pairs for the upcoming group test. Leaf really wants to know the pairs ahead of time... can you help him and predict the next output of Dr. J's prng? Leaf is pretty sure that Dr. J is using the middle-square method.
>
> nc shell.2019.nactf.com 31425
>
> The server is running the code in class-randomizer-0.c. Look at the function nextRand() to see how numbers are being generated!

The goal is to predict the next two random numbers for a flag. The server allows us to print some random outputs before guessing. Taking a look at the code, we can see that the next pseudorandom number is completely determined by the previous one.

```
uint64_t nextRand() {
  // Keep the 8 middle digits from 5 to 12 (inclusive) and square.
  float end = 12;
  seed = getDigits(seed, 5, 12);
  seed *= seed;
  return seed;
}
```

The nextRand() function uses the middle-square method: The 8 middle digits of the number are squared. So we can ask for a random output, square it, and get the next random number. Perform this twice to get the flag.

nactf{1_l0v3_chunky_7urn1p5}
