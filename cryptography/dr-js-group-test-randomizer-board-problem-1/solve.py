import socket

class sock:
    def __init__(self, host, port):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.connect((host, int(port)))

    def send(self, content):
        self.s.sendall(content.encode())
        data = ""
        data += self.s.recv(4096).decode('utf-8')
        return data

    def close(self):
        self.s.shutdown(1)
        self.s.close()


connection = sock("shell.2019.nactf.com", 31258)

print (connection.send("\n"))
print ("Waiting...")
history = []
for i in range(10):
    history.append(connection.send("r\n")[:-3])
lookup = {}
array = []
index = 0
while True:
    key = " ".join(history)
    if key in lookup:
        index = array.index(key)
        break
    else:
        lookup[key] = 1
        array.append(key)
    del history[0]
    nextRand = connection.send("r\n")[:-3]
    history.append(nextRand)

connection.send("g\n")
for i in range(4):
    if len(array) == index + 1:
        guess = array[index].split(" ")[9]
    else:
        guess = array[index+1+i].split(" ")[9]
    recv = connection.send(guess + "\n")
    print (recv)

connection.close()
