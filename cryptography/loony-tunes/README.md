# Loony Tunes

> Ruthie is very inhumane. She keeps her precious pigs locked up in a pen. I heard that this secret message is the password to unlocking the gate to her PIGPEN. Unfortunately, Ruthie does not want people unlocking the gate so she encoded the password. Please help decrypt this code so that we can free the pigs!
>
> P.S. "_" , "{" , and "}" are not part of the cipher and should not be changed
>
> P.P.S the flag is all lowercase

This is a pigpen cipher, a substitution cipher that works by placing each letter of the alphabet in a grid. Each letter is encrypted by drawing the adjacent lines in the grid. Reverse this encryption to get the flag.

nactf{th_th_th_thats_all_folks}