# Reversible Sneaky Algorithm #0

> Yavan sent me these really large numbers... what can they mean? He sent me the cipher "c", the private key "d", and the public modulus "n". I also know he converted his message to a number with ascii. For example:
>
> "nactf" --> \x6e61637466 --> 474080310374
>
> Can you help me decrypt his cipher?

This encryption uses the RSA public key cryptosystem. Messages are encrypted:

```
  c = m^e (mod n)
```

Ciphertexts are decrypted:

```
  m = c^d (mod n)
```

We are given c, d, and n, so we can go straight to decryption. Converting the resulting number to text gives the flag.

nactf{w3lc0me_t0_numb3r_th30ry}