# Reversible Sneaky Algorithm #1

> Lori decided to implement RSA without any security measures like random padding. Must be deterministic then, huh? Silly goose!
>
> She encrypted a message of the form nactf{****} where the redacted flag is a string of 4 lowercase alphabetical characters. Can you decrypt it?
> 
> As in the previous problem, the message is converted to a number by converting ascii to hex.

This challenge gives a public key with n and d, and asks us to decrypt a cipher c. Since n is too large to be factored, this would normally be impossible. 
However, we are given the format of the flag -- nactf{****}. This leaves 26^4 ≈ 500,000 options for the flag. This is a small enough number that we can
encrypt every possible flag and check if it equals the given cipher text. We iterate over every possible 4 character string and encrypt to get the flag.

nactf{pkcs}