# Super Duper AES

> The Advanced Encryption Standard (AES) has got to go. Spencer just invented the Super Duper Advanced Encryption Standard (SDAES), and it's 100% unbreakable. AES only performs up to 14 rounds of substitution and permutation, while SDAES performs 10,000. That's so secure, SDAES doesn't even use a key!

This challenge involves a homemade implementation of a substitution-permutation (SP) block cipher. 
An SP cipher performs repeated rounds of substitution and permutation, and XORs a secret key each round. 
This implememtation does not use a key to xor, so the substitution and permutation can be done in reverse. 

For instance, if one substitution rule is to replace any 4 with a 7, then the new rule should be to replace any 7 with a 4. 
Similarly, if a permutation rule is to move the 3rd bit to the 10th bit, then the new rule should be to move the 10th bit back to the 3rd bit. 

Reversal of the rules for both the substitution and permutation boxes can be accomplished by calculating the inverse permutation arrays 
and substituting them in the encryption code. Finally, the order of substitution and permutation should be swapped so that the 
steps are performed in the opposite order.

nactf{5ub5t1tut10n_p3rmutat10n_n33d5_a_k3y}