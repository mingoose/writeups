# least significant avenger

> I hate to say it but I think that Hawkeye is probably the Least Significant avenger. 
> Can you find the flag hidden in this picture?

The problem title hits at least significant bit stenography.
LSB stenography is when a hidden message is hidden in the least significant bit of every pixel.
Luckily, there is a website that can decode a LSB encrypted image for us!
    https://stylesuxx.github.io/steganography/
    





We got the flag! nactf{h4wk3y3_15_th3_l34st_51gn1f1c4nt_b1t}