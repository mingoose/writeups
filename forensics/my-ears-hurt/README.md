> The20thDuck sent me this really annoying audio file. It's way too high pitched to be his voice. What he is trying to tell me? Maybe its a code; he is the crypto master after all.

> You may have to convert file types.

> You will need to insert the string into the nactf{...} form before submitting.


This problem is very straight forward but tedious. If we listen to the audio file, it immediately sounds like morse code. 
There are many ways to decode it, but I thoguht by hand was simple enough.
I opened the file in audacity to make it easier to see if a sound was a dot or a dash and then I proceeded to decode.

After decoding I got this: d1dud0th15byh4nd
This means that our flag is nactf{d1dud0th15byh4nd}